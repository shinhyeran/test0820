import React, {Component} from "react";
class Test3 extends Component{
    constructor(props){
        super(props);

        let str = "-1 -2 -3 -4";
        let a = this.solution(str);
        this.props.answer(a)
    }

    solution = (str) => {
        let arr = str.split(" ")
        let min = Math.min.apply(null, arr)
        let max = Math.max.apply(null,arr)

        // return min +" " +max;
        return Math.min(...arr) + ' ' + Math.max(...arr);
    }
    
    render(){
        return null;
    }
}

export default Test3; 