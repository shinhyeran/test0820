import React, {Component} from 'react';

class Test1 extends Component {
    constructor(props){
        super(props);
        const value = {
            answer1_1: 'var : 변수 중복선언&재할당 가능, let : 재할당 불가능 , const : 상수',
            answer1_2: '가변객체(원시타입 이외), 불변객체(자바스크립트의 원시타입)',
            answer1_3: 'Promise : 콜백문제를 해결, 유연한 비동기 처리 가능, 코드 유지보수성이 증가 , callback : 코드의 복잡성 증가, 콜백지옥에 빠질 수 있음',
            answer1_4: 'this 바인딩 시 매개변수가 call은 콤마로 apply는 배열로 들어감',
            answer1_5: 'null : 비어있는 값 , undefined : 정의되지 않은 값 , undeclared : 선언되지 않은 값 ',
        }
        this.props.answer(value)
    }
    render () {
        return null;
    }
}

export default Test1;
