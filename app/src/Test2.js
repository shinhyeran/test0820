import React, {Component} from "react";
class Test2 extends Component{
    constructor(props){
        super(props);

        let participant = ["marina", "josipa", "nikola", "vinko", "filipa"]
        let completion = ["josipa", "filipa", "marina", "nikola"]

        let a = this.solution(participant, completion);
        this.props.answer(a)

    }

    solution = (par, com) => {
        if(par.length < 1 || par.length > 100000){
            return false
        }
        par.sort()
        com.sort()

        for(let i =0 ; i < par.length ; i++){
            if(par[i] != com[i]){
                return par[i]
            }
        }
    }
    
    render(){
        return null;
    }
}

export default Test2 ; 