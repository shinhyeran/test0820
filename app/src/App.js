import React from 'react';
import './App.css';
import Test1 from './Test1';
import Test2 from './Test2';
import Test3 from './Test3';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answer1_1: '',
      answer1_2: '',
      answer1_3: '',
      answer1_4: '',
      answer1_5: '',
      answer2: '',
      answer3: '',
    }
  }
  
      
  handleChange = (value) =>{
    this.setState({
      answer1_1 : value.answer1_1,
      answer1_2 : value.answer1_2,
      answer1_3 : value.answer1_3,
      answer1_4 : value.answer1_4,
      answer1_5 : value.answer1_5,
    })
  }
  
  handleChange2=(value)=>{
    this.setState({
      answer2:value
    })
  }
  
  handleChange3=(value)=>{
    this.setState({
      answer3:value
    })
  }


  render(){
    return (
      <div className="App">
        <header className="App-header">
          <Test1 answer={this.handleChange}/>
          <Test2 answer={this.handleChange2}/>
          <Test3 answer={this.handleChange3}/>
          <b1>지원자의 입력 답변</b1>
          <br/>
          <b1>TEST1</b1>
          <b1>답변1-1 : {this.state.answer1_1}</b1>
          <b1>답변1-2 : {this.state.answer1_2}</b1>
          <b1>답변1-3 : {this.state.answer1_3}</b1>
          <b1>답변1-4 : {this.state.answer1_4}</b1>
          <b1>답변1-5 : {this.state.answer1_5}</b1>
          <br/>
          <b1>TEST2</b1>
          <b1>답변2 : {this.state.answer2}</b1>
          <br/>
          <b1>TEST3</b1>
          <b1>답변3 : {this.state.answer3}</b1>
        </header>
      </div>
    );
  }
}